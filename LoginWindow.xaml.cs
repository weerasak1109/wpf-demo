﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAppDemo.Utility;

namespace WpfAppDemo
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {

        public LoginWindow()
        {
            InitializeComponent();
        }

        private bool cUserPass(string iPass)
        {
            try
            {
                string nPass = new Encryption().encrypt(iPass);
                if (!nPass.Equals(ConstManager.passTest)) 
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(iUserName.Text.Trim()))
                {
                    MessageBox.Show(ConstManager.MsgErrorUserNameNull, ConstManager.MsgTitle);
                }

                else if (string.IsNullOrEmpty(iPass.Password.Trim()))
                {
                    MessageBox.Show(ConstManager.MsgErrorPasswordNull, ConstManager.MsgTitle);
                }

                else if (cUserPass(iPass.Password) == true)
                { // login สำเร็จ
                    this.Hide();
                    MainWindow w = new MainWindow();
                    //w.ShowDialog(); ยังไม่รู้ใช้งานยังไง
                    w.Show();
                }
                else
                {
                    MessageBox.Show(ConstManager.MsgErrorLogin, ConstManager.MsgTitle);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// ออกโปรแกรม
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }
    }
}
