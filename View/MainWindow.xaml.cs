﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAppDemo.Models;

namespace WpfAppDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow(UserModels mUser =  null)
        {
            InitializeComponent();
        }

        /// <summary>
        /// กลับหน้าการขาย
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void iHome_MouseDown(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("Hello, world!", "My App");
        }

        /// <summary>
        /// ยอดขาย
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void iTotal_MouseDown(object sender, MouseButtonEventArgs e)
        {
         
        }

        /// <summary>
        /// ข้อมูลโปรแกรม
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void iInfo_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }
    }
}
