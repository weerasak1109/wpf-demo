﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfAppDemo.Utility
{
    public class ConstManager
    {
        /// <summary>
        /// ฝากไว้ก่อน
        /// </summary>
        public const string passTest = "O/Rjs64VLFI=";

        /// <summary>
        /// Error Title
        /// </summary>
        public const string MsgTitle = "I Shop !!!";

        /// <summary>
        /// กรุณากรอก UserName
        /// </summary>
        public const string MsgErrorUserNameNull = "กรุณากรอก UserName";

        /// <summary>
        /// กรุณากรอก Password
        /// </summary>
        public const string MsgErrorPasswordNull = "กรุณากรอก Password";

        /// <summary>
        /// เข้าสู่ระบบไม่สำเร็จ
        /// </summary>
        public const string MsgErrorLogin = "เข้าสู่ระบบไม่สำเร็จ";

 
    }
}
