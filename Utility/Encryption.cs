﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace WpfAppDemo.Utility
{
    public class Encryption
    {
        private const string cryptoKey = "czsqO+DxnA1EcyurkKdllA==";
        private static readonly byte[] IV = new byte[8] { 240, 3, 45, 29, 0, 76, 173, 59 };

        /// <summary>
        /// เข้ารหัส
        /// </summary>
        /// <param name="s">password</param>
        /// <returns></returns>
        public string encrypt(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }

            string result = string.Empty;
            try
            {
                byte[] buffer = Encoding.Default.GetBytes(s);
                TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
                MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();

                des.Key = MD5.ComputeHash(ASCIIEncoding.UTF32.GetBytes(cryptoKey));
                des.IV = IV;
                result = Convert.ToBase64String(
                    des.CreateEncryptor().TransformFinalBlock(
                        buffer, 0, buffer.Length));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// ถอดรหัส
        /// </summary>
        /// <param name="s">password</param>
        /// <returns></returns>
        public string decrypt(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }

            string result = string.Empty;

            try
            {
                byte[] buffer = Convert.FromBase64String(s);

                TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
                MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();
                des.Key = MD5.ComputeHash(ASCIIEncoding.UTF32.GetBytes(cryptoKey));
                des.IV = IV;
                result = Encoding.Default.GetString(
                    des.CreateDecryptor().TransformFinalBlock(
                    buffer, 0, buffer.Length));

            }
            catch (Exception ex)
            {
                throw ex; 
            }
            return result;
        }
    }
}
