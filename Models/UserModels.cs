﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfAppDemo.Models
{
    /// <summary>
    /// ข้อมูล ผู้ใช้งาน
    /// </summary>
    public class UserModels
    {
        /// <summary>
        /// รหัส
        /// </summary>
        public string userName { get; set; }
        /// <summary>
        /// ชื่อจริง
        /// </summary>
        public string fName { get; set; }
        /// <summary>
        /// นามสกุล
        /// </summary>
        public string lName { get; set; }
        /// <summary>
        /// เบอร์
        /// </summary>
        public string iTel { get; set; }
        /// <summary>
        /// อีเมล
        /// </summary>
        public string iEmail { get; set; }
    }
}
