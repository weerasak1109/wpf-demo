# WPF DEMO

ถูกจัดทำขึ้นเพื่อศึกษาและ ใช้จัดเก็บผลงาน


URL ที่ใช้ศึกษา
- https://material.io/resources/color/#!/?view.left=0&view.right=0&primary.color=212121 (material design colors)
- https://www.wpf-tutorial.com/panels/dockpanel/        (DockPanel control)
- https://www.wpf-tutorial.com/common-interface-controls/menu-control/   (WPF Menu control)  
- https://www.wpf-tutorial.com/dialogs/the-messagebox/      (MessageBox) 
- https://www.wpf-tutorial.com/panels/grid-rows-and-columns/    (Grid - Rows & columns)

